from django.urls import path
from flash_card_app.views import flash_card_list,create_flashcard

urlpatterns = [
    path("",flash_card_list,name="show_flashcard"),
    path("create/",create_flashcard,name="create_flashcard"),
]
