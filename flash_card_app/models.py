from django.db import models

# Create your models here.
class Flash_card(models.Model):
    name = models.CharField(max_length=100,null=True)
    front = models.CharField(max_length=200)
    back = models.TextField(max_length=1000)
